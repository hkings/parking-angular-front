import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { AlertModule } from 'ngx-bootstrap';
import { AppComponent } from './app.component';


import {HttpModule} from '@angular/http';
import { IngresarFormReactiveComponent } from './ingresar-form-reactive/ingresar-form-reactive.component'
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSelectModule} from '@angular/material/select';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material';
import {MatDialogModule } from '@angular/material';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { TicketComponent } from './ticket/ticket.component';
import { TicketserviceService } from './ticketservice.service';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { ClockComponent } from './clock/clock.component';
import { ActionconfirmationComponent } from './actionconfirmation/actionconfirmation.component';
import {ToastModule} from 'ng2-toastr/ng2-toastr';




@NgModule({
  declarations: [
    AppComponent,


    IngresarFormReactiveComponent,
    TicketComponent,
    ClockComponent,
    ActionconfirmationComponent,


  ],
  imports: [
    ToastModule.forRoot(),MatProgressSpinnerModule,MatDialogModule,MatPaginatorModule,MatTableModule,MatSelectModule,BrowserAnimationsModule,HttpModule,ReactiveFormsModule,BrowserModule,FormsModule,AlertModule.forRoot(),NgbModule.forRoot()
  ],
  providers: [TicketserviceService],
  bootstrap: [AppComponent],
  entryComponents: [TicketComponent,ActionconfirmationComponent]
})
export class AppModule { }
