import { Component, OnInit,ViewChild ,ViewContainerRef} from '@angular/core';
import { FormGroup,FormControl,Validators} from '@angular/forms'
import { PlacaValidators } from './placa.validators';
import { Http, Headers } from '@angular/http';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {NgbModal,NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { TicketComponent } from '../ticket/ticket.component';
import { ActionconfirmationComponent } from '../actionconfirmation/actionconfirmation.component';
  import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-ingresar-form-reactive',
  templateUrl: './ingresar-form-reactive.component.html',
  styleUrls: ['./ingresar-form-reactive.component.css']
})
export class IngresarFormReactiveComponent  {

  fileNameDialogRef: MatDialogRef<ActionconfirmationComponent>;

  isLoading=false;


  constructor(private http: Http,private dialog: MatDialog,public toastr: ToastsManager, vcr: ViewContainerRef) {

    this.toastr.setRootViewContainerRef(vcr);
      this.isLoading=true;
      this.reload(http);
  }

  ngOnInit(){

  }


  showError() {
        this.toastr.error('This is not good!', 'Oops!');
      }

  posts: any[];
  letter: any;
  id:string;
  uri:string;
  checkdia:any;

  tiposVeh=[
  {id:1,clase:'Carro'},
  {id:2,clase:'Moto'},

];

  form=new FormGroup({
      placa:new FormControl('',[
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(6),
      PlacaValidators.cannotContainSpace
    ],PlacaValidators.shouldBeUnique

  ),
    tipo: new FormControl('',Validators.required)
  ,
    cc:new FormControl('',Validators.required)
  });


  createPost(input: HTMLInputElement,input2:HTMLInputElement) {

    let post = { license: input.value,cc: input2.value.toString() };


    this.letter = post;
    input.value='';
    input2.value=null;
    const headers = new Headers({ 'Content-Type': 'application/json' });
    this.http.post('http://localhost:8183/api/vehicules', this.letter, { headers: headers })
      .subscribe(response => {


          this.reload(this.http);
        console.log(response.toString())
      },(error:Response)=>{

      //  this.checkdia=JSON.parse();
        this.checkdia=  error.json()
          debugger
        if(this.checkdia.message==='no puede ingresar por dia invalido'){
          this.showError();
          alert('No puede Ingresar placa no permitida este dia')
        }

        if(this.checkdia.message==='no puede ingresar por capacidad Motos'){

          alert('No puede Ingresar el parqueadero de motos esta lleno')
        }

        if(this.checkdia.message==='no puede ingresar por capacidad Carros'){

          alert('No puede Ingresar el parqueadero de carros esta lleno')
        }



      });

  }



     confirmacion(post){
       this.fileNameDialogRef=this.dialog.open(ActionconfirmationComponent,{
         data:post
       })

      this.fileNameDialogRef.afterClosed().subscribe(()=>{
        this.reload(this.http);
      })


     }


    reload(http){
      http.get('http://localhost:8183/api/vehiculespark')
        .subscribe(response => {
            this.isLoading=false;
          this.posts = response.json();


        },error =>{
          alert('Un error inesperado ha ocurrido.')
          console.log(error);
        });
    }

    cambioTipo(valordelTipo){
      if(valordelTipo==='Moto'){
        return true;
      }else{return false}
    }

    login(){
      this.form.setErrors({
        invalidLogin:true
      });

    }


    get placa(){
      return this.form.get('placa');
    }

}
