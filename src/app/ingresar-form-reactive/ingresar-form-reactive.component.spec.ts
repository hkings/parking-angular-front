import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresarFormReactiveComponent } from './ingresar-form-reactive.component';

describe('IngresarFormReactiveComponent', () => {
  let component: IngresarFormReactiveComponent;
  let fixture: ComponentFixture<IngresarFormReactiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresarFormReactiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresarFormReactiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
