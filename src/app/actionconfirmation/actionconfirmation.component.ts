import { Component, OnInit ,ElementRef,Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Http, Headers } from '@angular/http';
import { TicketComponent } from '../ticket/ticket.component';



@Component({
  selector: 'app-actionconfirmation',
  templateUrl: './actionconfirmation.component.html',
  styleUrls: ['./actionconfirmation.component.css']
})
export class ActionconfirmationComponent implements OnInit {

datar:any
posts: any[];
letter: any;
id:string;
uri:string;

fileNameDialogReff: MatDialogRef<TicketComponent>;

  constructor(@Inject(MAT_DIALOG_DATA)data,private elementRef: ElementRef,private http: Http,private dialog: MatDialog) {

    console.log("Data",data);
    this.datar=data;

  }
  reload(http){
    http.get('http://localhost:8183/api/vehiculespark')
      .subscribe(response => {
          //this.isLoading=false;
        this.posts = response.json();


      });
  }
    updatePost(post){
    const headers = new Headers({ 'Content-Type': 'application/json' });
    this.id=post.idVehicule;
    this.uri='http://localhost:8183/api/vehicules/';
    this.uri=this.uri.concat(this.id);
    this.http.patch(this.uri,post,{ headers: headers })
    .subscribe(response =>{

            
        this.fileNameDialogReff=this.dialog.open(TicketComponent,{
          data:response.text()

        });




    });

  }

  ngOnInit() {
  }

}
