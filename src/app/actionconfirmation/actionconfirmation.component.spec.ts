import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionconfirmationComponent } from './actionconfirmation.component';

describe('ActionconfirmationComponent', () => {
  let component: ActionconfirmationComponent;
  let fixture: ComponentFixture<ActionconfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionconfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionconfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
